﻿namespace TrueKitchen.Infra.Logging.Tests.Helpers
{
    public class DefaultServiceContext
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
