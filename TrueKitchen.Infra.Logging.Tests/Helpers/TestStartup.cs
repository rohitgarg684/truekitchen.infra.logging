﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;

namespace TrueKitchen.Infra.Logging.Tests.Helpers
{
    public class TestStartup
    {
        public IConfiguration Configuration { get; }
        public IHostingEnvironment Environment { get; }

        public TestStartup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            Environment = env;
        }

        //this method gets called at runtime
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            //order is important
            app.UseMvc(routes => { routes.MapRoute(name: "default", template: "api/{controller}/{action}/{id?}"); });
        }

        //this method gets called at runtime.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            var containerBuilder = new ContainerBuilder();

            containerBuilder
                .Register(c => new DefaultServiceContext {UserId =10, FirstName ="fTest" , LastName = "LTest" })
                .AsSelf();

            containerBuilder.AddLoggingServices("test-app");

            containerBuilder.Populate(services);
            var container = containerBuilder.Build();
            var serviceProvider = new AutofacServiceProvider(container);

            var logFactory = serviceProvider.GetRequiredService<ILoggerFactory>();
            NlogConfiguration.RegisterLogger(logFactory);

            return serviceProvider;
        }
    }
}
