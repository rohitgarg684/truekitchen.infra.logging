﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;

namespace TrueKitchen.Infra.Logging.Tests.Helpers
{
    public class TestServerFactory
    {
        public static TestServer GetServer()
        {
            var webHostBuilder = WebHost.CreateDefaultBuilder()
                 .ConfigureAppConfiguration((builderContext, config) => { config.AddEnvironmentVariables("TrueKitchen_"); })
                 .UseStartup<TestStartup>();
                 
            var server = new TestServer(webHostBuilder);
            return server;
        }
    }
}
