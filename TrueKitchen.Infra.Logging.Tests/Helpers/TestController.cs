﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace TrueKitchen.Infra.Logging.Tests.Helpers
{
    public class TestController: Controller
    {
        private readonly ILogger _log;

        public TestController(ILogger log)
        {
            _log = log;
        }

        public IEnumerable<string> Values()
        {
            _log.LogInformation("Returning Values");
            return new List<string> { "value1", "value2" };
        }
    }
}
