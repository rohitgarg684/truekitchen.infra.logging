﻿using Microsoft.AspNetCore.TestHost;
using System.Threading.Tasks;
using TrueKitchen.Infra.Logging.Tests.Helpers;
using Xunit;

namespace TrueKitchen.Infra.Api.Tests
{
    public class TestLogging
    {
        private readonly TestServer _testServer;
        public TestLogging()
        {
            _testServer = TestServerFactory.GetServer();
        }

        [Fact]
        public void Test_CorrelationId_Middleware()
        {
            var client = _testServer.CreateClient();
            var response = client.GetAsync(client.BaseAddress +"api/test/values").Result;
        }
    }

}
