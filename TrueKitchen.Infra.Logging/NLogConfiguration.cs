﻿using System;
using System.Collections.Generic;
using System.IO;
using Autofac;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using NLog.AWS.Logger;
using NLog.Config;
using NLog.Extensions.Logging;
using NLog.LayoutRenderers;
using TrueKitchen.Infra.Logging.Layout;

namespace TrueKitchen.Infra.Logging
{
    public static class NlogConfiguration
    {
        public static void RegisterLogger(ILoggerFactory loggerFactory) 
        {
            loggerFactory.AddNLog(new NLogProviderOptions
            {
                CaptureMessageTemplates = true,
                CaptureMessageProperties = true
            });

            LayoutRenderer.Register<ApplicationLayoutRenderer>(LoggingConstants.LayoutRenderer);
            
            //further registering aws target programitcally because Nlog is not picking awsTarget type
            var awsTarget = new AWSTarget()
            {
                Name = "aws",
                LogGroup = $"app.{LoggingConstants.ApplicationName}",
                Region = LoggingConstants.AWSRegionForLogging,
                MaxQueuedMessages= 10000,
                BatchSizeInBytes=100,
                BatchPushInterval= new TimeSpan(0,0,10),
                Layout = LoggingConstants.LayoutRenderer,
                OptimizeBufferReuse = true,
            };

            NLog.LogManager.Configuration = new XmlLoggingConfiguration(Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "nlog.config"));
            NLog.LogManager.Configuration.LoggingRules.Add(new LoggingRule(LoggingConstants.ApplicationName, NLog.LogLevel.Info, awsTarget));
           
        }

        public static void AddLoggingServices(this ContainerBuilder containerBuilder, string applicationName)
        {
            LoggingConstants.ApplicationName = applicationName;
            containerBuilder.RegisterType<LoggerFactory>()
                .As<ILoggerFactory>()
                .SingleInstance();

            containerBuilder.Register<ILogger>(c =>
            {
                var loggerFactory = c.Resolve<ILoggerFactory>();
                return loggerFactory.CreateLogger(LoggingConstants.ApplicationName);
            });
        }

        private static Dictionary<string,string> LoadContextVariables<TContext>(IComponentContext componentContext) where TContext : class, new()
        {
            var context = componentContext.ResolveOptional<TContext>();
            if (context != null)
            {
                var kvps = JsonConvert.DeserializeObject<Dictionary<string, string>>(JsonConvert.SerializeObject(context));
                return kvps;
            }

            return null;
        }
    }
}
