﻿namespace TrueKitchen.Infra.Logging
{
    internal class LoggingConstants
    {
        public static string LoggerName = "app";
        public static string ApplicationName = "app";
        public static string AWSRegionForLogging => "us-east-1";
        public static string LayoutRenderer => "application-layout-default";
    }
}
