﻿using System;
using System.Text;
using Newtonsoft.Json;
using NLog;
using NLog.LayoutRenderers;

namespace TrueKitchen.Infra.Logging.Layout
{
    //https://github.com/NLog/NLog/wiki/How-to-write-a-custom-layout-renderer
    [LayoutRenderer("application-layout-default")]
    public class ApplicationLayoutRenderer : LayoutRenderer
    {
        private readonly ExceptionLayoutRenderer _exceptionRenderer;

        public ApplicationLayoutRenderer()
        {
            _exceptionRenderer = new ExceptionLayoutRenderer();
            var exceptionFormat = "Message,Type,Method,StackTrace";
            _exceptionRenderer.Format = exceptionFormat;
            _exceptionRenderer.InnerFormat = exceptionFormat;
            _exceptionRenderer.MaxInnerExceptionLevel = 15;
        }

        protected override void Append(StringBuilder builder, LogEventInfo logEvent)
        {
            var log = LogManager.GetLogger("app");

            var msg = new ApplicationLogMessage
            {
                //global variables for the request
                Component = GlobalDiagnosticsContext.Get("Component"),
                IP = GlobalDiagnosticsContext.Get("IP"),
                MachineName = GlobalDiagnosticsContext.Get("MachineName"),

                //local variables for the request
                LoggerName = logEvent.LoggerName,
                Time = logEvent.TimeStamp,
                LogLevel = logEvent.Level.ToString().ToUpperInvariant(),
                Message = logEvent.FormattedMessage,
                Exception = logEvent?.Exception != null? _exceptionRenderer.Render(logEvent):null
            };

            var json = JsonConvert.SerializeObject(msg, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            builder.Append(json);
        }
    }
}
