﻿using System;

namespace TrueKitchen.Infra.Logging.Layout
{
    internal class ApplicationLogMessage
    {
        public string LoggerName { get; set; }

        public DateTime Time { get; set; }

        public string LogLevel { get; set; }

        public string Message { get; set; }

        public string Component { get; set; }

        public string IP { get; set; }

        public string MachineName { get; set; }

        public string CorrelationId { get; set; }

        public string HttpRequestUrl { get; set; }

        public string HttpMethod { get; set; }

        public string Exception { get; set; }

        public string HttpResponseStatusCode { get; internal set; }

        public string ResponseTime { get; set; }

        public string UserId { get; internal set; }

    }
}
